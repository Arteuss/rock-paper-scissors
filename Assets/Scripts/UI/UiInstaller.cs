﻿using DefaultNamespace;
using UnityEngine;
using WindowSystem;

namespace UI
{
    public class UiInstaller : BaseMonoInstaller
    {
        [SerializeField] private MainMenuView _mainMenuView;
        [SerializeField] private PlayerPanelView _playerPanelView;
        [SerializeField] private ResultWindowView _resultWindowView;
        
        protected override void DeclareSignals()
        {
            
        }

        protected override void BindManagers()
        {
            Container.BindInterfacesAndSelfTo<PlayerPanelController>().AsSingle();
            Container.BindInterfacesAndSelfTo<MainMenuController>().AsSingle();
            Container.BindInterfacesAndSelfTo<GameController>().AsSingle();
            Container.BindInterfacesAndSelfTo<ResultWindowController>().AsSingle();
        }

        protected override void BindPrefabs()
        {
            BindPrefab(_playerPanelView);
            BindPrefab(_mainMenuView);
            BindPrefab(_resultWindowView);
        }

        protected override void BindOther()
        {
            Container.Bind<GameControllerView>().FromComponentInHierarchy();
        }
    }
}
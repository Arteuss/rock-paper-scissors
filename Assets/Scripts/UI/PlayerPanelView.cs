﻿using System.Collections;
using Enum;
using Sounds;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.UI;
using WindowSystem.Views;

namespace UI
{
    public class PlayerPanelView : AbstractWindowView
    {
        [HideInInspector]
        [SerializeField] private bool  _unfairMode;
        public bool UnfairMode{get { return _unfairMode; }
            set { _unfairMode = value; }
        }

        [HideInInspector]
        [SerializeField] private float  _probabilityVictory = 0.5f;
        public float ProbabilityVictory{get { return _probabilityVictory; }
            set { _probabilityVictory = value; }
        }

        [SerializeField] private Text _сountdownText;
        [SerializeField] private Text _scoreText;

        private PlayerButton[] _selectionButtons;
        private SoundsManager _soundsManager;

        public void Listen(UnityAction<TypeObject> onClick, SoundsManager soundsManager)
        {
            _soundsManager = soundsManager;
            _selectionButtons = GetComponentsInChildren<PlayerButton>();
            int count = _selectionButtons.Length;
            for (int i = 0; i < count; i++)
            {
                _selectionButtons[i].Listen(onClick);
            }

            _сountdownText.text = "";
        }

        public override void Unlisten()
        {
            int count = _selectionButtons.Length;
            for (int i = 0; i < count; i++)
            {
                _selectionButtons[i].Unlisten();
            }
        }

        public void SetInteractiveButtons(bool value)
        {
            foreach (PlayerButton button in _selectionButtons)
            {
                button.SetInterective(value);
            }
        }

        public void StartChoose()
        {
            SetInteractiveButtons(false);
            StartCoroutine(StartCountdown());
        }

        private IEnumerator StartCountdown()
        {
            yield return new WaitForSeconds(3f);
            for (int i = 0; i < 3; i++)
            {
                _soundsManager.Counting();
                _сountdownText.text = (i+1).ToString();
                yield return new WaitForSeconds(0.5f);
            }
            _сountdownText.text = "";
        }
        
        public void SetPrevScore(int playerScore, int aiScore)
        {
            _scoreText.text = playerScore + " : " + aiScore;
        }

        public void SetScore(int playerScore, int aiScore)
        {
            StartCoroutine(Score(playerScore, aiScore));
        }

        private IEnumerator Score(int playerScore, int aiScore)
        {
            yield return new WaitForSeconds(1f);
            _scoreText.text = playerScore + " : " + aiScore;
        }
    }
}
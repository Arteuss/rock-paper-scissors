﻿using System.Linq;
using Enum;
using UnityEngine;

namespace Sounds
{
    [CreateAssetMenu(menuName = "Sounds/SoundsDatabase", fileName = "SoundsDatabase")]
    public class SoundsDatabase: ScriptableObject
    {
        [SerializeField] private ItemSound [] _sounds;
        
        public AudioClip GetClip(TypeSound uid)
        {
            return _sounds.SingleOrDefault(f => f.Uid == uid).Clip;
        }
    }
}
﻿using Sounds;
using UnityEngine;
using WindowSystem.Managers;

namespace UI
{
    public class MainMenuController : AbstractWindowManager
    {
        private readonly MainMenuView _view;
        private readonly WindowsManager _windowsManager;
        private readonly PlayerPanelController _playerPanelController;
        private readonly SoundsManager _soundsManager;

        public MainMenuController(
            MainMenuView view,
            WindowsManager windowsManager,
            PlayerPanelController playerPanelController,
            SoundsManager soundsManager
            ) : base(view)
        {
            _view = view;
            _windowsManager = windowsManager;
            _playerPanelController = playerPanelController;
            _soundsManager = soundsManager;
        }

        public override void Listen()
        {
           _view.Listen(OnStartClick, OnResetClick);
           _view.Open();
        }

        void OnStartClick()
        {
            _windowsManager.OpenWindow<PlayerPanelController>();
            _soundsManager.Button();
        }
        void OnResetClick()
        {
            _windowsManager.OpenWindow<PlayerPanelController>();
            _playerPanelController.SetPrevScore(0, 0);
            PlayerPrefs.DeleteAll();
            _soundsManager.Button();
        }
    }
}
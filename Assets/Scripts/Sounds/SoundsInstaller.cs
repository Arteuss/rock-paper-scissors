﻿using Zenject;

namespace Sounds
{
    public class SoundsInstaller: MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<SoundsManager>().AsSingle().NonLazy();
        }
    }
}
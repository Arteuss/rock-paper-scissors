﻿using WindowSystem.Interfaces;

namespace WindowSystem.Managers
{
    public abstract class AbstractWindowManager : IWindowManager
    {
        private readonly IWindowView _view;

        protected AbstractWindowManager
        (
            IWindowView view
        )
        {
            _view = view;
        }

        public void Open()
        {
            _view.Open();
        }

        public void Close()
        {
            _view.Close();
        }

        public abstract void Listen();

        public void Unlisten()
        {
            _view.Unlisten();
        }
    }
}
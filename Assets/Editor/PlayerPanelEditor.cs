﻿using UI;
using UnityEditor;

namespace Editor
{
    [CustomEditor(typeof(PlayerPanelView))]
    public class PlayerPanelEditor : UnityEditor.Editor
    {
        private PlayerPanelView _playerPanel;
        
        public override void OnInspectorGUI()
        {
            _playerPanel = (PlayerPanelView) target;
            _playerPanel.UnfairMode = EditorGUILayout.Toggle("Нечестный режим", _playerPanel.UnfairMode);
            
            if (_playerPanel.UnfairMode)
            {
                ShowProbably();
            }
            EditorGUILayout.LabelField(
                "_________________________________________________________________________________________________");
            EditorGUILayout.Space();
            
            DrawDefaultInspector();
            
            serializedObject.ApplyModifiedProperties();
        }

        private void ShowProbably()
        {
            EditorGUILayout.LabelField("Вероятность победы оппонента");
            _playerPanel.ProbabilityVictory =
                EditorGUILayout.Slider("Вероятность", _playerPanel.ProbabilityVictory, 0f, 1f);
        }
    }
}

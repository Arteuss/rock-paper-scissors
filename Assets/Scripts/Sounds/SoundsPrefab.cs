﻿using UnityEngine;
using Zenject;

namespace Sounds
{
    [CreateAssetMenu(fileName = "SoundsSource", menuName = "Installers/SoundsPrefab")]
    public class SoundsPrefab: ScriptableObjectInstaller
    {
        [SerializeField] private SoundsView _soundsSource;
        [SerializeField] private SoundsDatabase _soundsDatabase;

        public override void InstallBindings()
        {
            Container.Bind<SoundsView>().FromComponentInNewPrefab(_soundsSource).AsSingle();
            Container.BindInstance(_soundsDatabase).AsSingle();
        }
    }
}
﻿using UnityEngine;
using UnityEngine.Audio;
using Zenject;

namespace Sounds
{
    public class SoundsView: MonoBehaviour
    {
        [SerializeField] private AudioSource _source;
        [SerializeField] private AudioSource _sourceSecond;

        public void Play(AudioClip clip, bool isLooping)
        {
            if (!_source.isPlaying)
            {
                _source.clip = clip;
                _source.loop = isLooping;
                _source.Play();
            }
            else
            {
                _sourceSecond.clip = clip;
                _sourceSecond.loop = isLooping;
                _sourceSecond.Play();
            }
        }

        public void Stop()
        {
            _source.Stop();
            _sourceSecond.Stop();
        }
    }
}
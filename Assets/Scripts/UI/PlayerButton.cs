﻿using System;
using Enum;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI
{
    public class PlayerButton : MonoBehaviour
    {
        [SerializeField] private TypeObject _typeObject;
        private Button _button;
        private UnityAction<TypeObject> _onClick;

        public void Listen(UnityAction<TypeObject> onClick)
        {
            _onClick = onClick;
            _button = GetComponent<Button>();
            _button.onClick.AddListener(OnClick);
        }

        public void Unlisten()
        {
            _onClick = null;
            _button.onClick.RemoveAllListeners();
        }

        void OnClick()
        {
            _onClick(_typeObject);
        }

        public void SetInterective(bool value)
        {
            _button.enabled = value;
        }
    }
}
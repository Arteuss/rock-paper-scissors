﻿using Sounds;
using WindowSystem.Interfaces;
using WindowSystem.Managers;

namespace UI
{
    public class ResultWindowController : AbstractWindowManager, IOpen<int>
    {
        private readonly ResultWindowView _view;
        private readonly WindowsManager _windowsManager;
        private readonly SoundsManager _soundsManager;

        public ResultWindowController(
            ResultWindowView view,
            WindowsManager windowsManager,
            SoundsManager soundsManager
            ) : base(view)
        {
            _view = view;
            _windowsManager = windowsManager;
            _soundsManager = soundsManager;
        }

        public override void Listen()
        {
            _view.Listen(OnNextClick, OnMenuClick);
        }

        private void OnNextClick()
        {
            _windowsManager.OpenWindow<PlayerPanelController>();
            _soundsManager.Button();
        }

        private void OnMenuClick()
        {
            _windowsManager.OpenWindow<MainMenuController>();
            _soundsManager.Button();
        }

        public void Open(int result)
        {
            _view.SetTexts(result, _soundsManager);
        }

        public void SetPrevResult(int pScore, int aiScore)
        {
           _view.SetResult(pScore, aiScore);
        }
    }
}
﻿using WindowSystem.Managers;

namespace WindowSystem.Installers
{
    public class WindowSystemInstaller : BaseMonoInstaller
    {
        protected override void DeclareSignals()
        {
            
        }

        protected override void BindManagers()
        {
            Container.BindInterfacesAndSelfTo<WindowsFabric>().AsSingle();
            Container.BindInterfacesAndSelfTo<WindowsManager>().AsSingle();
        }

        protected override void BindPrefabs()
        {
            
        }

        protected override void BindOther()
        {
            
        }
    }
}
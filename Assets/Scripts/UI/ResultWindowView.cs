﻿using System.Collections;
using Sounds;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using WindowSystem.Views;

namespace UI
{
    public class ResultWindowView : AbstractWindowView
    {
        [SerializeField] private Text _title;
        [SerializeField] private Text _playerScore;
        [SerializeField] private Text _compScore;
        [SerializeField] private Button _next;
        [SerializeField] private Button _menu;
        private int _pScore;
        private int _aiScore;

        public void Listen(UnityAction onNextClick, UnityAction onMenuClick)
        {
            _next.onClick.AddListener(onNextClick);
            _menu.onClick.AddListener(onMenuClick);
        }
        
        public override void Unlisten()
        {
            _next.onClick.RemoveAllListeners();
            _menu.onClick.RemoveAllListeners();
        }

        //Параметр result принимает три значения 1 - победа, 2 - поражение, 0 - ничья
        public void SetTexts(int result, SoundsManager sound)
        {
            switch (result)
            {
                case 1:
                    sound.Win();
                    _title.text = "You Win!";
                    _title.color = Color.green;
                    _playerScore.text = _pScore.ToString();
                    _compScore.text = _aiScore.ToString();
                    _pScore++;
                    break;
                case 2:
                    sound.Lose();
                    _title.text = "You Lose!";
                    _title.color = Color.red;
                    _playerScore.text = _pScore.ToString();
                    _compScore.text = _aiScore.ToString();
                    _aiScore++;
                    break;
                case 0:
                    sound.Draw();
                    _title.text = "DRAW";
                    _title.color = Color.gray;
                    _playerScore.text = _pScore.ToString();
                    _compScore.text = _aiScore.ToString();
                    break;
            }

            StartCoroutine(Result());
        }

        public void SetResult(int pScore, int aiScore)
        {
            _pScore = pScore;
            _aiScore = aiScore;
        }

        private IEnumerator Result()
        {
            yield return new WaitForSeconds(0.7f);
            _playerScore.text = _pScore.ToString();
            _compScore.text = _aiScore.ToString();
        }
    }
}
﻿using System;
using Enum;
using UI;
using UnityEngine;
using WindowSystem.Managers;
using Zenject;

namespace DefaultNamespace
{
    public class GameController : IInitializable, IDisposable
    {
        private readonly GameControllerView _view;
        private readonly WindowsManager _windowsManager;

        private GameController(
            GameControllerView view,
            WindowsManager windowsManager
        )
        {
            _view = view;
            _windowsManager = windowsManager;
            
        }
        
        public void Initialize()
        {
            _windowsManager.OpenWindow<MainMenuController>();
            Listen();
        }

        private void Listen()
        {
            _view.Listen();
        }

        void Unlisten()
        {
            _view.Unlisten();
        }

        public void Dispose()
        {
            Unlisten();
        }

        public void StartChoose(TypeObject obj, bool unfair, float probability)
        {
            _view.StartOpponentChoose(obj, unfair, probability);
        }

        public void SetActions(Action<int> roundResult)
        {
            _view.SetActions(roundResult);
        }
    }
}
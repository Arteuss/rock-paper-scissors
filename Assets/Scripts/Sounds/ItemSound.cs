﻿using System;
using Enum;
using UnityEngine;

namespace Sounds
{
    [Serializable]
    public class ItemSound
    {
        public TypeSound Uid;
        public AudioClip Clip;
    }
}
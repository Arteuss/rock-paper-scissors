﻿using System;
using System.Collections;
using System.Collections.Generic;
using Enum;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameControllerView : MonoBehaviour
{
	[SerializeField] private GameObject[] _handPrefabs;
	private List<GameObject> _aiObjects = new List<GameObject>();
	private List<GameObject> _playerObjects = new List<GameObject>();

	private Action<int> _roundResult;
	private int _objChose;

	public void Listen()
	{
		int count = _handPrefabs.Length;
		for (int i = 0; i < count; i++)
		{
			GameObject obj = Instantiate(_handPrefabs[i]);
			obj.transform.parent = gameObject.transform;
			Vector3 pos = new Vector3(obj.transform.position.x + 1f, obj.transform.position.y,
				obj.transform.position.z);
			Vector3 scale = new Vector3();
			if (i < 2)
			{
				scale = new Vector3(obj.transform.localScale.x * -1f, obj.transform.localScale.y,
					obj.transform.localScale.z);
			}
			else
			{
				scale = new Vector3(obj.transform.localScale.x, obj.transform.localScale.y,
					obj.transform.localScale.z * -1f);
			}

			obj.transform.position = pos;
			obj.transform.localScale = scale;
			obj.SetActive(false);
			_aiObjects.Add(obj);
			
			GameObject pObj = Instantiate(_handPrefabs[i]);
			pObj.transform.parent = gameObject.transform;
			Vector3 pPos = new Vector3(pObj.transform.position.x - 1.5f, pObj.transform.position.y,
				pObj.transform.position.z);
			pObj.transform.position = pPos;
			pObj.SetActive(false);
				_playerObjects.Add(pObj);
		}
	}

	public void Unlisten()
	{
		int count = _aiObjects.Count;
		for (int i = 0; i < count; i++)
		{
			Destroy(_aiObjects[i]);
			Destroy(_playerObjects[i]);
		}
		_aiObjects.Clear();
		_playerObjects.Clear();
	}

	public void SetActions(Action<int> roundResult)
	{
		_roundResult = roundResult;
	}

	public void StartOpponentChoose(TypeObject obj, bool unfair, float probability)
	{
		_playerObjects[(int)obj].SetActive(true);
		StartCoroutine(FairChoose(obj, unfair, probability));
	}

	private IEnumerator FairChoose(TypeObject obj, bool unfair, float prob)
	{
		foreach (GameObject aiObject in _aiObjects){aiObject.SetActive(false);}
		yield return new WaitForSeconds(0.7f);

		int count = _aiObjects.Count;
		for (int i = 0; i < count; i++)
		{
			_aiObjects[i].SetActive(true);
			yield return new WaitForSeconds(0.7f);
//			if(i < count-1)
				_aiObjects[i].SetActive(false);
		}
		
		if (unfair)
		{
			float p = prob * 100;
			float x = Random.Range(0f, 100f);
			if (x > 0 && x <= p || p == 100f)
			{
				switch (obj)
				{
					case TypeObject.Stone:
						_objChose = 2;
						break;
					case TypeObject.Scissors:
						_objChose = 0;
						break;
					case TypeObject.Paper:
						_objChose = 1;
						break;
				}
				yield return new WaitForSeconds(1.7f);
				_aiObjects[_objChose].SetActive(true);
				yield return new WaitForSeconds(1f);
				_roundResult(2);
				Debug.Log("---Поражение---");
			}

			if (x > p && x <= 100 || p == 0f)
			{
				int y = Random.Range(0, 2);
				if (p == 0f) y = 1;
				
				if (y == 0)
				{
					switch (obj)
					{
						case TypeObject.Stone:
							_objChose = 0;
							break;
						case TypeObject.Scissors:
							_objChose = 1;
							break;
						case TypeObject.Paper:
							_objChose = 2;
							break;
					}
					yield return new WaitForSeconds(1.7f);
					_aiObjects[_objChose].SetActive(true);
					yield return new WaitForSeconds(1f);
					_roundResult(0);
					Debug.Log("---Ничья---");
				}
				else
				{
					switch (obj)
					{
						case TypeObject.Stone:
							_objChose = 1;
							break;
						case TypeObject.Scissors:
							_objChose = 2;
							break;
						case TypeObject.Paper:
							_objChose = 0;
							break;
					}
					yield return new WaitForSeconds(1.7f);
					_aiObjects[_objChose].SetActive(true);
					yield return new WaitForSeconds(1f);
					_roundResult(1);
					Debug.Log("---Победа---");
				}
			}
		}
		else
		{
			_objChose = Random.Range(0, 3);
			yield return new WaitForSeconds(1.7f);
			_aiObjects[_objChose].SetActive(true);
			switch (obj)
			{
				case TypeObject.Stone:
					if (_objChose == 0)
					{
						yield return new WaitForSeconds(1f);
						_roundResult(0);
						Debug.Log("---Ничья---");
					}

					if (_objChose == 1)
					{
						yield return new WaitForSeconds(1f);
						_roundResult(1);
						Debug.Log("---Победа---");
					}

					if (_objChose == 2)
					{
						yield return new WaitForSeconds(1f);
						_roundResult(2);
						Debug.Log("---Поражение---");
					}

					break;
				case TypeObject.Scissors:
					if (_objChose == 0)
					{
						yield return new WaitForSeconds(1f);
						_roundResult(2);
						Debug.Log("---Поражение---");
					}

					if (_objChose == 1)
					{
						yield return new WaitForSeconds(1f);
						_roundResult(0);
						Debug.Log("---Ничья---");
					}

					if (_objChose == 2)
					{
						yield return new WaitForSeconds(1f);
						_roundResult(1);
						Debug.Log("---Победа---");
					}

					break;
				case TypeObject.Paper:
					if (_objChose == 0)
					{
						yield return new WaitForSeconds(1f);
						_roundResult(1);
						Debug.Log("---Победа---");
					}

					if (_objChose == 1)
					{
						yield return new WaitForSeconds(1f);
						_roundResult(2);
						Debug.Log("---Поражение---");
					}

					if (_objChose == 2)
					{
						yield return new WaitForSeconds(1f);
						_roundResult(0);
						Debug.Log("---Ничья---");
					}

					break;
			}
		}

		yield return new WaitForSeconds(1f);
		foreach (GameObject aiObject in _aiObjects){aiObject.SetActive(false);}
		foreach (GameObject pObject in _playerObjects){pObject.SetActive(false);}
	}

	[ContextMenu("Удалить сохранения")]
	public void ClearSave()
	{
		PlayerPrefs.DeleteAll();
	}
}

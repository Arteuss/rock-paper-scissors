﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using WindowSystem.Views;

namespace UI
{
    public class MainMenuView : AbstractWindowView
    {
        [SerializeField] private Button _startButton;
        [SerializeField] private Button _resetButton;
        

        public void Listen(UnityAction onStart, UnityAction onReset)
        {
            _startButton.onClick.AddListener(onStart);
            _resetButton.onClick.AddListener(onReset);
        }

        public override void Unlisten()
        {
            _startButton.onClick.RemoveAllListeners();
            _resetButton.onClick.RemoveAllListeners();
        }

        public void Open()
        {
            if (PlayerPrefs.HasKey("ScorePlayer") || PlayerPrefs.HasKey("ScoreAi"))
                _resetButton.gameObject.SetActive(true);
            else
                _resetButton.gameObject.SetActive(false);
        }
    }
}
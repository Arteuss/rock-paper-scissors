﻿using Enum;

namespace Sounds
{
    public class SoundsManager
    {
        private readonly SoundsView _view;
        private readonly SoundsDatabase _soundsDatabase;

        public SoundsManager(SoundsView view, SoundsDatabase soundsDatabase)
        {
            _view = view;
            _soundsDatabase = soundsDatabase;
        }

        private void Play(TypeSound Uid, bool isLooping = false)
        {_view.Play(_soundsDatabase.GetClip(Uid), isLooping);}
        
        public void Stop()
        {_view.Stop();}

        public void Button()
        { Play(TypeSound.Button);}

        public void Win()
        { Play(TypeSound.Win);}

        public void Lose()
        {Play(TypeSound.Lose);}

        public void Draw()
        {Play(TypeSound.Draw);}
        
        public void Counting()
        {Play(TypeSound.Counting);}

        public float Duration(TypeSound uid)
        {
            return _soundsDatabase.GetClip(uid).length;
        }
    }
}
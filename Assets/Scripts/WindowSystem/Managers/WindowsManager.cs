﻿using System.Collections.Generic;
using WindowSystem.Interfaces;

namespace WindowSystem.Managers
{
    public class WindowsManager
    {
        private readonly Stack<IWindowManager> _stack = new Stack<IWindowManager>();
        private readonly WindowsFabric _windowsFabric;
        
        public WindowsManager
        (
            WindowsFabric windowsFabric
        )
        {
            _windowsFabric = windowsFabric;
        }
        
        public void OpenWindow<T>() where T : IWindowManager
        {
            if (_stack.Count > 0)
            {
                _stack.Peek().Unlisten();
                _stack.Peek().Close();
            }
            var window = _windowsFabric.Window<T>();
            Open(window);
        }

        public void OpenWindow<T, TX>(TX data) where T : IWindowManager, IOpen<TX>
        {
            OpenWindow<T>();
            ((IOpen<TX>) _stack.Peek()).Open(data);
        }

        public void CloseWindow()
        {
            Close();
        }

        public void PopWindow<T> () where T : IWindowManager
        {
            _stack.Peek().Unlisten();
            var window = _windowsFabric.Window<T>();
            Open(window);
        }

        private void Open(IWindowManager window)
        {
            _stack.Push(window);
            _stack.Peek().Open();
            _stack.Peek().Listen();
        }

        private void Close()
        {
            _stack.Peek().Unlisten();
            _stack.Pop().Close();
            _stack.Peek().Open();
            _stack.Peek().Listen();
        }
    }
}
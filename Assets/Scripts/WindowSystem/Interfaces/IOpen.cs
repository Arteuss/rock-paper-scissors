﻿namespace WindowSystem.Interfaces
{
	public interface IOpen<T>
	{
		void Open(T data);
	}
}
﻿using DefaultNamespace;
using Enum;
using Sounds;
using UnityEngine;
using WindowSystem.Managers;

namespace UI
{
    public class PlayerPanelController : AbstractWindowManager
    {
        private readonly PlayerPanelView _view;
        private readonly GameController _gameController;
        private readonly ResultWindowController _resultWindowController;
        private readonly WindowsManager _windowsManager;
        private readonly SoundsManager _soundsManager;

        private int ScorePlayer
        {
            get { return PlayerPrefs.GetInt("ScorePlayer", 0); }
            set { PlayerPrefs.SetInt("ScorePlayer", value); }
        }
        
        private int ScoreAi
        {
            get { return PlayerPrefs.GetInt("ScoreAi", 0); }
            set { PlayerPrefs.SetInt("ScoreAi", value); }
        }

        public PlayerPanelController(
            PlayerPanelView view,
            GameController gameController,
            ResultWindowController resultWindowController,
            WindowsManager windowsManager,
            SoundsManager soundsManager
            ) : base(view)
        {
            _view = view;
            _gameController = gameController;
            _resultWindowController = resultWindowController;
            _windowsManager = windowsManager;
            _soundsManager = soundsManager;
        }

        public override void Listen()
        {
            _view.Listen(StartOpponentStep, _soundsManager);
            _gameController.SetActions(RoundResult);
            SetPrevScore(ScorePlayer, ScoreAi);
        }

        public void SetPrevScore(int pScore, int aiScore)
        {
            _view.SetPrevScore(pScore, aiScore);
        }

        void StartOpponentStep(TypeObject obj)
        {
            _gameController.StartChoose(obj, _view.UnfairMode, _view.ProbabilityVictory);
            _view.StartChoose();
            _soundsManager.Button();
        }

        //Параметр result принимает три значения 1 - победа, 2 - поражение, 0 - ничья
        void RoundResult(int result)
        {
            _resultWindowController.SetPrevResult(ScorePlayer, ScoreAi);
            switch (result)
            {
                case 1:
                    ScorePlayer++;
                    break;
                case 2:
                    ScoreAi++;
                    break;
                case 0:
                    break;
            }
            _view.SetScore(ScorePlayer, ScoreAi);
            _windowsManager.OpenWindow<ResultWindowController, int>(result);
            _view.SetInteractiveButtons(true);
        }
    }
}